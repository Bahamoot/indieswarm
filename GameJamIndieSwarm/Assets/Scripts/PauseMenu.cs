﻿using UnityEngine;
using System.Collections;

public class PauseMenu : MonoBehaviour {


	public bool pause = false;
	public GameObject PauseScreen;

	// Use this for initialization
	void Start () {

		PauseScreen = transform.FindChild ("PauseScreen").gameObject;
		PauseScreen.gameObject.SetActive (false);

	
	}
	
	// Update is called once per frame
	void Update () {

		if (pause == false) {

			Time.timeScale = 1;
			PauseScreen.gameObject.SetActive (false);

			if(Input.GetButtonDown ("Pause")){

				pause = true;

			}


		} else if (pause == true) {

			Time.timeScale = 0;
			PauseScreen.gameObject.SetActive (true);

			if(Input.GetButtonDown ("Pause")){

				pause = false;

			}

		}

	
	}

	public void QuitGame (){
		
		
		Application.Quit ();
		
	}

	public void ReturnToMenu(){

		Application.LoadLevel ("Menu");
		
		
	}

	public void ReturnToGame (){

		pause = false;


	}

}
