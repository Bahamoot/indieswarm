﻿using UnityEngine;
using System.Collections;

public class MenuSystem : MonoBehaviour {

	

	public GameObject StartScreen;
	public GameObject MainMenuScreen;
	public GameObject CreditsScreen;

	public enum MenuStates{

		StartScreen,
		MainMenuScreen,
		CreditsScreen

	}

	MenuStates CurrentMenu;


	void Start () {

		CurrentMenu = MenuStates.StartScreen;

		StartScreen.gameObject.SetActive (true);
		MainMenuScreen.gameObject.SetActive (false);
		CreditsScreen.gameObject.SetActive (false);
	
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.anyKeyDown) {

			ReturnToMenu();

		}
	
	}

	public void GoToCreditsScreen(){

		StartScreen.gameObject.SetActive (false);
		MainMenuScreen.gameObject.SetActive (false);
		CreditsScreen.gameObject.SetActive (true);
	}

	public void ReturnToMenu(){

		StartScreen.gameObject.SetActive (false);
		MainMenuScreen.gameObject.SetActive (true);
		CreditsScreen.gameObject.SetActive (false);
	

	}

	public void PlayGame(){

		Application.LoadLevel ("Level1");


	}

	public void QuitGame (){


		Application.Quit ();

	}

}
